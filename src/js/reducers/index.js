import { combineReducers } from 'redux';
import allStudents from '../reducers/allStudents';
import sortReducer from './sortReducer';

// const allReducers = combineReducers({
//     StudentsData:allStudents 
// })

const allReducers = combineReducers({
    StudentsData:allStudents,
    sortByNames: sortReducer
})

export default allReducers;
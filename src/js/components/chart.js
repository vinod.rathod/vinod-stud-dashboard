import React, {Component} from 'react';
import { Bar } from 'react-chartjs-2';

class Chart extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            chartData : {
                labels: ['One', 'Two', 'Three'],
                datasets : [
                    {
                        label: '',
                        data: [1000,2000,3000],
                        backgroundColor: ['red','green','blue']
                    }
                ]
            }
        }

    }

    render(){
        return(
            <div>
                <Bar 
                    data = {this.state.chartData}
                    options = {{
                        title: {
                            display: true,
                            text: 'Student marks'
                        },
                        legend: {
                            display: true,
                            position: 'right'
                        }
                    }}
                />
            </div>
        );
    }
}

export default Chart;
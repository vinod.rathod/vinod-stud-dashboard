import React from 'react';
import MenuBar from './menubar';
import Display from './display';
import Details from './details';
import { NavLink, Route, BrowserRouter,Link } from 'react-router-dom';

const App= () => (
     <BrowserRouter>
    <div>
        <MenuBar />
        {/* <Display /> */}
        <Route exact path="/" component={Display} />
        {/* <Route path="/display/:category" component={Display} /> */}
        <Route path="/details/:id" component={Details} /> 
        {/* <Route path="/home" component={this} /> */}
    </div>
     </BrowserRouter>
);

export default App;
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink, Router, BrowserRouter,Link } from 'react-router-dom';
require('../../scss/style.css');

class Display extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: {},
            keys: []
        }
    }

    componentDidMount() {
        console.log(this.props.allStudents)
        this.props.allStudents.then(result=> {
                    this.setState({
                        isLoaded: true,
                        items: result,
                        keys: Object.keys(result)
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    // componentDidMount() {
    //     fetch("https://api.myjson.com/bins/1dlper")
    //         .then(res => res.json())
    //         .then(
    //             (result) => {
    //                 this.setState({
    //                     isLoaded: true,
    //                     items: result,
    //                     keys: Object.keys(result)
    //                 });
    //             },
    //             (error) => {
    //                 this.setState({
    //                     isLoaded: true,
    //                     error
    //                 });
    //             }
    //         )
    // }

    render() {
        console.log(this.props)
        const { error, isLoaded, items, keys } = this.state;
        // console.log(items);
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="grid-container">
                    {
                        keys.map(key => {
                            return (
                                // <div className="cards">
                                 <NavLink className="cards" to={`/details/${items[key].rollNo}`} key={items[key].rollNo}> 
                                 
                                <h5>{items[key].rollNo}</h5>
                                <h5>{items[key].name}</h5>
                                <h5>{items[key].class}</h5>
                                {console.log(items[key].name)}
                                
                                 </NavLink> 
                            //     </div>
                             );
                            
                        })
                    }
                    {/* <Cards items={items} keys={keys} /> */}
                </div>
            );
        }
    }
}

function mapStateToProps(state){
    return(
    {
        allStudents: state.StudentsData
    })
}

export default connect(mapStateToProps)(Display);
import React from 'react';
import Highcharts from 'highcharts';

class Chart extends React.Component {
    constructor(props){
        super(props);
        console.log(this.props.array);
        this.state = {
             series: [{
                  name: 'Marks',
                   data: this.props.array
                 }]
        }
        
    }

    highChartsRender() {
        Highcharts.chart({
            chart: {
              type: 'column',
              renderTo: 'marks-graph'
            },
            title: {
              horizontalAlign: 'middle',
              floating: true,
              text: 'Marks scored by student',
              style: {
                  fontSize: '10px',
              }
            },
            plotOptions: {
              column: {
                  dataLabels: {
                      format: '{point.name}: {point.percentage:.1f} %'
                  },
                innerSize: '50%'
              }
            },
            series: this.state.series

          
           });
    }

    componentDidMount() {
        this.highChartsRender();
      }

    render() {
        return (
            <div id="marks-graph">
            </div>
        );
      }
}

export default Chart;
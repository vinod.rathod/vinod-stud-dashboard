import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { sortByNames } from '../actions/index';
import { bindActionCreators } from 'redux';
require('../../scss/style.css');

class MenuBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
        name : "ascending",
        mark : "ascending",
        student : {}
    }
}

componentDidMount(){
  this.props.students.then(data =>{
      this.setState({
          student : data
      })
  })
}

    render() {
      console.log(this.state.student)
      return (
        <div className="menu">
          <input type="text" name="search" value="" placeholder="search" onChange="" />
          <li name="sortbynames"  onClick = {()=>{this.props.sort(this.state.student,this.state.name);this.changeName()}}>Sort by names</li>
          <button name="sortbymarks">Sort by marks</button>
          {/* <NavLink className="links" to={`/home`}><h4>Home</h4></NavLink> */}
        </div>
      );
    }

    changeName = () => {
      if(this.state.name === "ascending")
          this.setState({ name : "descending" } )
      else
          this.setState({ name : "ascending" } )
  }

  }

  function matchDispatchToProps(dispatch) {
    //  selectCategory();
    return bindActionCreators({ sort : sortByNames },dispatch)
}



function stateToProps(state) {
    // console.log(state.studentDetails)
    return ({ 
        students : state.StudentsData
    });

}





export default connect(stateToProps,matchDispatchToProps)(MenuBar);



  // export default MenuBar;
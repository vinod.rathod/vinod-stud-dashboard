import React, { Component } from 'react';
import { connect } from 'react-redux';
import Chart from './newChart';
import Axios from 'axios';


require('../../scss/style.css');

export class Details extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: {},
            keys: [],
            id:''
        }
    }


     componentDidMount(){
         console.log("student id is");
        // console.log(this.props.allStudents);
         console.log(this.props.match.params.id);
        if(this.props.match.params.id){
        this.props.allStudents.then(result => {
             console.log("in fetch");
             console.log(result);
             
            this.setState({
                isLoaded: true,
                items: result[this.props.match.params.id],
                keys: Object.keys(result),
                id: this.props.match.params.id
            });
            // console.log("in component did mount");
            // console.log(this.state.items);
        }, error => {})
    }
    }

    getObjectArray(obj) {
        let arr = [];
        let keys = Object.keys(obj);
        keys.forEach(key => {
            arr.push({
                name: key,
                y: obj[key],
                color: '#3498db'
            });
        });
        console.log(arr);
        return arr;
    }


    render() {
        
        // console.log(this.props.match.params.id)
         console.log("in render");
          console.log(this.state.items);
         const { error, isLoaded, items, keys } = this.state;
        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div className="stud-details">
                    <div className="inner-div">
                        <div className="details">
                            <h4>Id:&nbsp;{items.rollNo}</h4>
                            <h4>Name:&nbsp;{items.name}</h4>
                            <h4>Grade:&nbsp;{items.class}</h4>
                        </div>

                        <div className="chart">
                            <Chart array={this.getObjectArray(items.marks)} name={items.name} />
                        </div>

                    </div>

                </div>
            );
        }
    }
}

function mapStateToProps(state) {
    return (
        {allStudents: state.StudentsData}
            )
}


export default connect(mapStateToProps)(Details);

//export default Details;


